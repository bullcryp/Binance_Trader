const binance = require('node-binance-api');
const moment = require('moment');

var request = require('xhr-request')
var async = require('async')

var redis = require("redis"),
    redis_client = redis.createClient();
var redis_expiry_seconds = 60; // expire set in seconds
var redis_cycle_seconds = 10; // expire set in seconds
var keys_prefix = "bina:"; // prefix all the redis keys

//var coinpair_object = {}; // { "btc_eth": [ aggregate_data, 12 hours ], "coin_pair": [...], ... };

var count = 0
var movers_object = {}
var binance_concurrency = 8
var binance_starttime // calculate elapsed time
var binance_coin_length = 289 // retreive 300 pairs of candlestic

//
// Async.js queues
//

// create a queue object with concurrency, connection to Binance API
var binance_async = async.queue(function(task, callback) {

	binance.candlesticks(task.coin, task.interval, (error, ticks, symbol) => {
		count++
		//console.log("candlesticks() count:", count, 'Coin:', task.coin, Date() );
		let last_tick = ticks[ticks.length - 1];
		//movers_object[task.coin] = movers_object[task.coin] || [] // initialize
		movers_object[task.coin] = ticks;
		let [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = last_tick;
		console.log(Date.now(), symbol, "Count:" + count, "last close: "+close+" Time:"+ moment(closeTime).toDate() );
		callback();
	}, {limit: task.limit }) //, endTime: 1514764800000});

}, binance_concurrency); // # of queues in parallel

// assign a callback
binance_async.drain = function() {
	console.log('Finished.'+ moment(Date.now()).toDate()+' Elapsed time:', (Date.now() - binance_starttime)/1000+ "s", "Binance Concurrency: "+binance_concurrency);
	console.log(movers_object) //
	console.log(generate_movers_data(movers_object))
	// Data points [current,5min,15min,2hr,4hr,8hr,12hr,24hr] = [0,1,3,6,24,48,96,144,288] 
};


// DB Stuff
// var mysql = require('mysql')

// var connection = mysql.createConnection({
// 	host: "localhost",
// 	user: 'polo_user',
// 	password: 'HeaVenLy',
// 	database: "binancedb",
// 	multipleStatements: true,
// 	timezone: 'utc'
//   });
//   connection.connect(function (err) {
// 	if (err) {
// 	  console.error('error connecting: ' + err.stack);
// 	  return;
// 	}
// 	console.log('connected as id ' + connection.threadId);
//   });

// if you'd like to select database 3, instead of 0 (default), call
// client.select(3, function() { /* ... */ });

redis_client.on("error", function (err) {
    console.log("Error " + err);
});


//$binance_apiKey = '80M72JH6mVTYTHWE3InVF9Z6V9AEJot6FaAbSYQrHmtoVLMxs6d0fjk3oOfkLBGb';
//$binance_apiSecret = 'DwSEyUK65WMjNMQZs98ayMaGl2eI8cTWg7OOGcG5BsgWnLWuCdmyofu0ZGZe76Dh';


binance.options({
  APIKEY: '80M72JH6mVTYTHWE3InVF9Z6V9AEJot6FaAbSYQrHmtoVLMxs6d0fjk3oOfkLBGb',
  APISECRET: 'DwSEyUK65WMjNMQZs98ayMaGl2eI8cTWg7OOGcG5BsgWnLWuCdmyofu0ZGZe76Dh',
  useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
  test: true // If you want to use sandbox mode where orders are simulated
});


//binance.websockets.chart("BNBBTC", "1m", (symbol, interval, chart) => {
//  let tick = binance.last(chart);
//  const last = chart[tick].close;
//  console.log(chart);
  // Optionally convert 'chart' object to array:
  // let ohlc = binance.ohlc(chart);
  // console.log(symbol, ohlc);
//  console.log(symbol+" last price: "+last)
//});

//var coins_array = ["ETHBTC","LTCBTC","BNBBTC","NEOBTC","123456","QTUMETH","EOSETH","SNTETH","BNTETH","BCCBTC","GASBTC","BNBETH","BTCUSDT","ETHUSDT","HSRBTC","OAXETH","DNTETH","MCOETH","ICNETH","MCOBTC","WTCBTC","WTCETH","LRCBTC","LRCETH","QTUMBTC","YOYOBTC","OMGBTC","OMGETH","ZRXBTC","ZRXETH","STRATBTC","STRATETH","SNGLSBTC","SNGLSETH","BQXBTC","BQXETH","KNCBTC","KNCETH","FUNBTC","FUNETH","SNMBTC","SNMETH","NEOETH","IOTABTC","IOTAETH","LINKBTC","LINKETH","XVGBTC","XVGETH","SALTBTC","SALTETH","MDABTC","MDAETH","MTLBTC","MTLETH","SUBBTC","SUBETH","EOSBTC","SNTBTC","ETCETH","ETCBTC","MTHBTC","MTHETH","ENGBTC","ENGETH","DNTBTC","ZECBTC","ZECETH","BNTBTC","ASTBTC","ASTETH","DASHBTC","DASHETH","OAXBTC","ICNBTC","BTGBTC","BTGETH","EVXBTC","EVXETH","REQBTC","REQETH","VIBBTC","VIBETH","HSRETH","TRXBTC","TRXETH","POWRBTC","POWRETH","ARKBTC","ARKETH","YOYOETH","XRPBTC","XRPETH","MODBTC","MODETH","ENJBTC","ENJETH","STORJBTC","STORJETH","BNBUSDT","VENBNB","YOYOBNB","POWRBNB","VENBTC","VENETH","KMDBTC","KMDETH","NULSBNB","RCNBTC","RCNETH","RCNBNB","NULSBTC","NULSETH","RDNBTC","RDNETH","RDNBNB","XMRBTC","XMRETH","DLTBNB","WTCBNB","DLTBTC","DLTETH","AMBBTC","AMBETH","AMBBNB","BCCETH","BCCUSDT","BCCBNB","BATBTC","BATETH","BATBNB","BCPTBTC","BCPTETH","BCPTBNB","ARNBTC","ARNETH","GVTBTC","GVTETH","CDTBTC","CDTETH","GXSBTC","GXSETH","NEOUSDT","NEOBNB","POEBTC","POEETH","QSPBTC","QSPETH","QSPBNB","BTSBTC","BTSETH","BTSBNB","XZCBTC","XZCETH","XZCBNB","LSKBTC","LSKETH","LSKBNB","TNTBTC","TNTETH","FUELBTC","FUELETH","MANABTC","MANAETH","BCDBTC","BCDETH","DGDBTC","DGDETH","IOTABNB","ADXBTC","ADXETH","ADXBNB","ADABTC","ADAETH","PPTBTC","PPTETH","CMTBTC","CMTETH","CMTBNB","XLMBTC","XLMETH","XLMBNB","CNDBTC","CNDETH","CNDBNB","LENDBTC","LENDETH","WABIBTC","WABIETH","WABIBNB","LTCETH","LTCUSDT","LTCBNB","TNBBTC","TNBETH","WAVESBTC","WAVESETH","WAVESBNB","GTOBTC","GTOETH","GTOBNB","ICXBTC","ICXETH","ICXBNB","OSTBTC","OSTETH","OSTBNB","ELFBTC","ELFETH","AIONBTC","AIONETH","AIONBNB","NEBLBTC","NEBLETH","NEBLBNB","BRDBTC","BRDETH","BRDBNB","MCOBNB","EDOBTC","EDOETH","WINGSBTC","WINGSETH","NAVBTC","NAVETH","NAVBNB","LUNBTC","LUNETH","TRIGBTC","TRIGETH","TRIGBNB","APPCBTC","APPCETH","APPCBNB","VIBEBTC","VIBEETH","RLCBTC","RLCETH","RLCBNB","INSBTC","INSETH","PIVXBTC","PIVXETH","PIVXBNB","IOSTBTC","IOSTETH","CHATBTC","CHATETH","STEEMBTC","STEEMETH","STEEMBNB","NANOBTC","NANOETH","NANOBNB","VIABTC","VIAETH","VIABNB","BLZBTC","BLZETH","BLZBNB","AEBTC","AEETH","AEBNB","RPXBTC","RPXETH","RPXBNB","NCASHBTC","NCASHETH","NCASHBNB","POABTC","POAETH","POABNB","ZILBTC","ZILETH","ZILBNB","ONTBTC","ONTETH","ONTBNB","STORMBTC","STORMETH","STORMBNB","QTUMBNB","QTUMUSDT","XEMBTC","XEMETH","XEMBNB","WANBTC","WANETH","WANBNB","WPRBTC","WPRETH","QLCBTC","QLCETH","SYSBTC","SYSETH","SYSBNB","QLCBNB","GRSBTC","GRSETH"];

// Initialize coins array
//
coins_array = []; 

// Get exchange information
//
binance.exchangeInfo(function(error, data) {
	data.symbols.forEach(function(elem) { 
		//if (elem.status == "TRADING") {
			coins_array.push(elem.symbol) 
		//}
	});
	//db_check_coinpair(coins_array)
	// console.log(JSON.stringify(coins_array));
	//startCollecting();
	startMovers();
	// process.exit();
});

// Main functions
//
// Generate Movers table

function startMovers() {
binance_starttime = Date.now();
	// Intervals: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
	for (coin of coins_array) {
		// binance_async.push({coin: coin, interval: "1m", limit: 600 }, function (err) {
		// 	//console.log('finished processing: '+coin);
		// });
		binance_async.push({coin: coin, interval: "5m", limit:  binance_coin_length}, function (err) {
			//console.log('finished processing: '+coin);
		});
		// Binance outputs: 
		// [
		// 	[ // data,            // [#] Description
		// 	  1499040000000,      // [0] Open time 
		// 	  "0.01634790",       // [1] Open
		// 	  "0.80000000",       // [2] High
		// 	  "0.01575800",       // [3] Low
		// 	  "0.01577100",       // [4] Close
		// 	  "148976.11427815",  // [5] Volume
		// 	  1499644799999,      // [6] Close time
		// 	  "2434.19055334",    // [7] Quote asset volume
		// 	  308,                // [8] Number of trades
		// 	  "1756.87402397",    // [9] Taker buy base asset volume
		// 	  "28.46694368",      // [10] Taker buy quote asset volume
		// 	  "17928899.62484339" // [11] Ignore
		// 	],
		//	....
		// ]
	}
}


function generate_movers_data(movers_object) {
	var movers_data = [], coin_data
	    
	for (coin in movers_object) { // interate over all coin names
		coin_data = [] // initialize coin data
		coin_data[0] = coin // put in coin value

		coin_candlestick = movers_object[coin].slice().reverse() // clones object, then reverses it
		var candlestick_datapoints = [1,3,6,24,48,96,144,coin_candlestick.length - 1] // generate the pointers
		var newest_coin_value = parseFloat(coin_candlestick[0][4]) // latest close price, not static

		if (coin_candlestick.length == binance_coin_length ) { // full candlestick
			for (datapts of candlestick_datapoints) {
				var current_coin_value = parseFloat(coin_candlestick[datapts][4])
				coin_data.push(Math.abs(100*(newest_coin_value - current_coin_value)/newest_coin_value)) // get % change
			}
		} else { // partial candlestick, extract when time crosses1

			var newest_closing_time = coin_candlestick[0][6] // closing time
			var candlestick_dataseconds = candlestick_datapoints.map(x => x * 5 * 60 * 1000) // convert to miliseconds
			var ptr = 0 // start pointer

			// cycle through the array and pick the time that crosses the seconds
			for (elem of coin_candlestick) { // run through the candlestick array
				var current_closing_time = elem[6]
				if ((newest_closing_time - current_closing_time) > candlestick_dataseconds[ptr]) { // time crossed
					var current_coin_value = parseFloat(elem[4])
					coin_data.push(Math.abs(100*(newest_coin_value - current_coin_value)/newest_coin_value)) 
					++ptr // increment pointer
				}
			}
			console.log(coin_data)
		}

		movers_data.push(coin_data);
	}
	return movers_data

}
//
//
// OLD CODING
//
//

// start collecting trades
// function startCollecting() {
// 	var prevTime, setsTime; // ordered set timestamp
// 	binance.websockets.trades(coins_array, (trades) => {
// 	  let {e:eventType, E:eventTime, s:symbol, p:price, q:quantity, m:maker, a:tradeId, T:tradeTime} = trades;
// 	  //var tradeT = moment.utc(tradeTime).;
// 	  var tradeT = new Date(tradeTime);
// 	  var eventT = new Date(eventTime);
// 	  var setsTime = Math.floor(tradeTime/redis_cycle_seconds/1000)*redis_cycle_seconds;
// 	  // on change in setTime, aggregrate the previous set time
// 	  if ( (prevTime != setsTime) ) {
// 		parseSet(prevTime);
// 		prevTime = setsTime;
// 	  }
// 	  //console.log(tradeT + " | " + eventT + " trade time: "+ symbol + " price: "+price+", quantity: "+quantity+", maker: "+maker);
// 	  //console.log(setsTime + " | " + eventTime + " trade time: "+ symbol + " price: "+price+", quantity: "+quantity+", maker: "+maker);
// 	  redis_client.zadd(keys_prefix + setsTime, eventTime, JSON.stringify(trades));
// 	});
// }


// process the sorted sets in redis
// function parseSet(redis_sets) {
// 	var aggregateItem, itemArray = [], sId, eId, sDate, eDate, sAmount = 0, sRate = 0, sTotal = 0, sCount = 0, bAmount = 0, bRate = 0, bTotal = 0, bCount = 0, endTradeDate, tmp, open, high, low, close

	
// 	if (redis_sets != undefined) { // sets key must be defined
// 		console.log( "Expiring "+redis_sets+" in "+redis_expiry_seconds+" seconds." );
// 		redis_client.expire(redis_sets, redis_expiry_seconds);


// 		redis_client.zrevrange(keys_prefix + redis_sets, 0, -1, function(err, set_members) {
// 			//var element = JSON.parse(members);
// 			//console.log(element.s)
// 			coinpair_object = {}; // initialize
// 		        set_members.forEach(function (data, index) {
// 					element = JSON.parse(data);
// 		//{e:eventType, E:eventTime, s:symbol, p:price, q:quantity, m:maker, a:tradeId, T:tradeTime} 

// 	//console.log(element.s); return;
// 					eDate = element.T
// 					eId = element.a
// 					price = parseFloat(element.p)

// 					if (coinpair_object[element.s] == undefined) { // first array, initialize

// 						coinpair_object[element.s] = {
// 							dateStart: eDate,
// 							dateEnd: eDate,
// 							startTradeId: eId,
// 							endTradeId: eId,
// 							high: price,
// 							low: price,
// 							close: price,
// 							sell: {
// 								price: price,
// 								amount: 0,
// 								total: 0,
// 								trades: 0
// 							},
// 							buy: {
// 								price: price,
// 								amount: 0,
// 								total: 0,
// 								trades: 0
// 							}
// 						}

// 					} else {
// 						coinpair_object[element.s].close = price
// 					}

// 					if (element.m == true) { // buyer-maker true == sell
// 						sRate = parseFloat(element.p)  // Price: only set first time, which is newest item
// 						coinpair_object[element.s].high = Math.max(sRate, coinpair_object[element.s].high)
// 						coinpair_object[element.s].low = Math.min(sRate, coinpair_object[element.s].low)
// 						coinpair_object[element.s].sell.amount += parseFloat(element.q)
// 						coinpair_object[element.s].sell.total += parseFloat(element.q) * parseFloat(element.p)
// 						coinpair_object[element.s].sell.trades += 1
// 					} else {
// 						bRate = parseFloat(element.p)
// 						coinpair_object[element.s].high = Math.max(bRate, coinpair_object[element.s].high)
// 						coinpair_object[element.s].low = Math.min(bRate, coinpair_object[element.s].low)
// 						coinpair_object[element.s].buy.amount += parseFloat(element.q)
// 						coinpair_object[element.s].buy.total += parseFloat(element.q) * parseFloat(element.p)
// 						coinpair_object[element.s].buy.trades += 1
// 					}


// 				})
// 			update_server(coinpair_object)
// 			return coinpair_object // initialize
// 		});

// 	} 
// }

// function update_server(coinpair_object) {


// }


// // DB methods

// function makeSQLCall(sql,callback){
// 	connection.query(sql,
// 	  function (err, res) {
// 		if(err){
// 		  console.log(err)
// 		}
// 		if(callback){
// 		  callback(err,res)
// 		}
// 	  }
// 	)
//   }

// function db_check_coinpair(coins_array) {
// 	coins_array.forEach(function(coin, index) {
// 		sql = "CREATE TABLE IF NOT EXISTS " + coin + " LIKE BTC_AMP";
// 		//console.log(sql)
// 		makeSQLCall(sql, function(err, res) {
// 			// handle error if exists
// 		})
// 	})
	
// }


function toUTC(t) {
	return moment.parseZone(t).utc().format();
}

